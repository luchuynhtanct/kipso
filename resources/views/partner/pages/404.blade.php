<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>404</title>
    <meta name="description"
        content="240+ Best Bootstrap Templates are available on this website. Find your template for your project compatible with the most popular HTML library in the world." />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="canonical" href="Replace_with_your_PAGE_URL" />

    <!-- Open Graph (OG) meta tags are snippets of code that control how URLs are displayed when shared on social media  -->
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Bucker – About Us" />
    <meta property="og:url" content="PAGE_URL" />
    <meta property="og:site_name" content="Bucker – About Us" />
    <!-- For the og:image content, replace the # with a link of an image -->
    <meta property="og:image" content="#" />
    <meta property="og:description" content="Bucker – About Us" />

    <!-- Add site Favicon -->
    <link rel="icon" href="https://hasthemes.com/wp-content/uploads/2019/04/cropped-favicon-32x32.png" sizes="32x32" />
    <link rel="icon" href="https://hasthemes.com/wp-content/uploads/2019/04/cropped-favicon-192x192.png"
        sizes="192x192" />
    <link rel="apple-touch-icon" href="https://hasthemes.com/wp-content/uploads/2019/04/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage"
        content="https://hasthemes.com/wp-content/uploads/2019/04/cropped-favicon-270x270.png" />

    <!-- CSS
    ========================= -->
    <link rel="stylesheet" href="/assets_404/assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="/assets_404/assets/css/slick.css">
    <link rel="stylesheet" href="/assets_404/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets_404/assets/css/ionicons.min.css">
    <link rel="stylesheet" href="/assets_404/assets/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/assets_404/assets/css/animate.css">
    <link rel="stylesheet" href="/assets_404/assets/css/nice-select.css">
    <link rel="stylesheet" href="/assets_404/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="/assets_404/assets/css/jquery-ui.min.css">
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="/assets_404/assets/css/style.css">

    <!--modernizr min js here-->
    <script src="/assets_404/assets/js/vendor/modernizr-3.11.2.min.js"></script>


    <!-- Structured Data  -->
    <script type="application/ld+json">
        {
        "@context": "https://schema.org",
        "@type": "WebSite",
        "name": "Replace_with_your_site_title",
        "url": "Replace_with_your_site_URL"
        }
    </script>
</head>

<body>


    <!--offcanvas menu area start-->
    <div class="body_overlay">

    </div>
    <div class="offcanvas_menu">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="offcanvas_menu_wrapper">
                        <div class="canvas_close">
                            <a href="javascript:void(0)"><i class="ion-android-close"></i></a>
                        </div>
                        <div class="welcome_text text-center">
                            <p>Welcome to Bucker Bakery Shop</p>
                        </div>
                        <div id="menu" class="text-left ">
                            <ul class="offcanvas_main_menu">
                                <li class="menu-item-has-children active">
                                    <a href="#">Home</a>
                                    <ul class="sub-menu">
                                        <li><a href="index.html">Home 01</a></li>
                                        <li><a href="index-2.html">Home 02</a></li>
                                    </ul>
                                </li>
                                <li><a href="about.html">About Us</a></li>
                                <li class="menu-item-has-children"><a href="#">Pages</a>
                                    <ul class="sub-menu">
                                        <li><a href="faq.html">FAQ</a></li>
                                        <li><a href="404.html">Error 404</a></li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="#">Shop</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children"><a href="#">Shop Layout</a>
                                            <ul class="sub-menu">
                                                <li>
                                                    <a href="shop-fullwidth.html">Shop Fullwidth</a>
                                                </li>
                                                <li>
                                                    <a href="shop-left-sidebar.html">Shop Left Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-right-sidebar.html">Shop Right Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-list-fullwidth.html">Shop List Fullwidth</a>
                                                </li>
                                                <li>
                                                    <a href="shop-list-left-sidebar.html">Shop List Left Sidebar</a>
                                                </li>
                                                <li>
                                                    <a href="shop-list-right-sidebar.html">Shop List Right
                                                        Sidebar</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">Product Style</a>
                                            <ul class="sub-menu">
                                                <li>
                                                    <a href="single-product.html">Single Product Default</a>
                                                </li>
                                                <li>
                                                    <a href="single-product-group.html">Single Product Group</a>
                                                </li>
                                                <li>
                                                    <a href="single-product-variable.html">Single Product
                                                        Variable</a>
                                                </li>
                                                <li>
                                                    <a href="single-product-sale.html">Single Product Sale</a>
                                                </li>
                                                <li>
                                                    <a href="single-product-sticky.html">Single Product Sticky</a>
                                                </li>
                                                <li>
                                                    <a href="single-product-affiliate.html">Single Product
                                                        Affiliate</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">Popular Products</a>
                                            <ul class="sub-menu">
                                                <li>
                                                    <a href="shop-left-sidebar.html">Classic Carrot Cake</a>
                                                </li>
                                                <li>
                                                    <a href="shop-left-sidebar.html">Gingerbread Cake</a>
                                                </li>
                                                <li>
                                                    <a href="shop-left-sidebar.html">Yellow Cupcakes</a>
                                                </li>
                                                <li>
                                                    <a href="shop-left-sidebar.html">Hawaiian Cake Roll</a>
                                                </li>
                                                <li>
                                                    <a href="shop-left-sidebar.html">Banana Snack Cake</a>
                                                </li>
                                                <li>
                                                    <a href="shop-left-sidebar.html">Chocolate Cake</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">Product Related</a>
                                            <ul class="sub-menu">
                                                <li>
                                                    <a href="my-account.html">My Account</a>
                                                </li>
                                                <li>
                                                    <a href="login-register.html">Login | Register</a>
                                                </li>
                                                <li>
                                                    <a href="cart.html">Shopping Cart</a>
                                                </li>
                                                <li>
                                                    <a href="wishlist.html">Wishlist</a>
                                                </li>
                                                <li>
                                                    <a href="compare.html">Compare</a>
                                                </li>
                                                <li>
                                                    <a href="checkout.html">Checkout</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="blog-fullwidth.html">blog</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item-has-children"><a href="#">Blog Holder</a>
                                            <ul class="sub-menu">
                                                <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                                <li><a href="blog-right-sidebar.html">Blog Right Sidebar</a></li>
                                                <li><a href="blog-fullwidth.html">Blog Fullwidth</a></li>
                                                <li><a href="blog-list-left-sidebar.html">Blog List Left Sidebar</a>
                                                </li>
                                                <li><a href="blog-list-right-sidebar.html">Blog List Right
                                                        Sidebar</a>
                                                </li>
                                                <li><a href="blog-list-fullwidth.html">Blog List Fullwidth</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item-has-children"><a href="#">Blog Details Holder</a>
                                            <ul class="sub-menu">
                                                <li><a href="blog-detail-fullwidth.html">Blog Detail Fullwidth</a>
                                                </li>
                                                <li><a href="blog-detail-left-sidebar.html">Blog Detail Left
                                                        Sidebar</a></li>
                                                <li><a href="blog-detail-right-sidebar.html">Blog Detail Right
                                                        Sidebar</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="contact.html">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--mini cart-->
    <div class="mini_cart">
        <div class="cart_gallery">
            <div class="cart_close">
                <div class="cart_text">
                    <h3>cart</h3>
                </div>
                <div class="mini_cart_close">
                    <a href="javascript:void(0)"><i class="ion-android-close"></i></a>
                </div>
            </div>
            <div class="cart_item">
                <div class="cart_img">
                    <a href="single-product.html"><img src="/assets_404/assets/img/product/product1.png" alt=""></a>
                </div>
                <div class="cart_info">
                    <a href="single-product.html">Primis In Faucibus</a>
                    <p>1 x <span> $65.00 </span></p>
                </div>
                <div class="cart_remove">
                    <a href="#"><i class="ion-android-close"></i></a>
                </div>
            </div>
            <div class="cart_item">
                <div class="cart_img">
                    <a href="single-product.html"><img src="/assets_404/assets/img/product/product2.png" alt=""></a>
                </div>
                <div class="cart_info">
                    <a href="single-product.html">Letraset Sheets</a>
                    <p>1 x <span> $60.00 </span></p>
                </div>
                <div class="cart_remove">
                    <a href="#"><i class="ion-android-close"></i></a>
                </div>
            </div>
        </div>
        <div class="mini_cart_table">
            <div class="cart_table_border">
                <div class="cart_total">
                    <span>Sub total:</span>
                    <span class="price">$125.00</span>
                </div>
                <div class="cart_total mt-10">
                    <span>total:</span>
                    <span class="price">$125.00</span>
                </div>
            </div>
        </div>
        <div class="mini_cart_footer">
            <div class="cart_button">
                <a href="cart.html">View cart</a>
            </div>
            <div class="cart_button">
                <a href="checkout.html"><i class="fa fa-sign-in"></i> Checkout</a>
            </div>
        </div>
    </div>
    <!--mini cart end-->

    <!-- page search box -->
    <div class="page_search_box">
        <div class="search_close">
            <i class="ion-close-round"></i>
        </div>
        <form class="border-bottom" action="#">
            <input class="border-0" placeholder="Search products..." type="text">
            <button type="submit"><span class="pe-7s-search"></span></button>
        </form>
    </div>
    <!-- Begin Error 404 Area -->
    <div class="error-404-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="error-404-content">
                        <h1 class="title mb-4">404</h1>
                        <h2 class="sub-title mb-4">Page Cannot Be Found!</h2>
                        <p class="short-desc mb-7">Seems like nothing was found at this location. Try something else or
                            you
                            can go back to the homepage following the button below!</p>
                        <div class="button-wrap">
                            <a class="btn btn-danger btn-lg rounded-0" href="index.html">Back to home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JS
============================================ -->

    <script src="/assets_404/assets/js/vendor/jquery-3.6.0.min.js"></script>
    <script src="/assets_404/assets/js/vendor/jquery-migrate-3.3.2.min.js"></script>
    <script src="/assets_404/assets/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="/assets_404/assets/js/slick.min.js"></script>
    <script src="/assets_404/assets/js/owl.carousel.min.js"></script>
    <script src="/assets_404/assets/js/wow.min.js"></script>
    <script src="/assets_404/assets/js/jquery.scrollup.min.js"></script>
    <script src="/assets_404/assets/js/jquery.nice-select.js"></script>
    <script src="/assets_404/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="/assets_404/assets/js/mailchimp-ajax.js"></script>
    <script src="/assets_404/assets/js/jquery-ui.min.js"></script>
    <script src="/assets_404/assets/js/jquery.zoom.min.js"></script>
    <!-- Main JS -->
    <script src="/assets_404/assets/js/main.js"></script>


</body>

</html>
